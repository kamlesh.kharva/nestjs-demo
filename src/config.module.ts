// src/config.module.ts
import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'mysql',
      host: '10.102.32.196',
      port: 3306,
      username: 'development',
      password: 'Admin@123',
      database: 'mytest_nest',
      autoLoadModels: true,
      synchronize: true, // Set to true only for development, to auto-create tables
    }),
  ],
})
export class ConfigModule {}
