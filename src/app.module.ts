import { Module, ValidationPipe } from '@nestjs/common';
import { APP_PIPE } from '@nestjs/core';
import { ConfigModule } from './config.module';
import { FormModule } from './form/form.module';

@Module({
  imports: [ConfigModule, FormModule],
  providers: [
    FormModule,
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
  ],
})
export class AppModule {}
