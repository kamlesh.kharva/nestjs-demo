import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Form } from './entity/form.entity';
import { Form_Data } from './entity/form_data.entity';
import { FormController } from './form.controller';
import { FormService } from './form.service';
import {Form_Field} from "./entity/form_field.entity";

@Module({
  imports: [SequelizeModule.forFeature([Form, Form_Data, Form_Field])],
  controllers: [FormController],
  providers: [FormService],
})
export class FormModule {}
