import { Injectable, BadRequestException  } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Form } from './entity/form.entity';
import { CreateFormDto } from './dto/create-form.dto';
import {Form_Field} from "./entity/form_field.entity";
import {Form_Data} from "./entity/form_data.entity";

@Injectable()
export class FormService {
  constructor(
      @InjectModel(Form)
      private readonly formModel: typeof Form,
      @InjectModel(Form_Field)
      private readonly formFieldModel: typeof Form_Field,
      @InjectModel(Form_Data)
      private readonly formDataModel: typeof Form_Data,
  ) {}

   /**
   * createForm() : - Create dynamic generate form.
   * @param fieldsData It's array data which is pass in DTO object function.
   * @returns The Form object Insert.
   */
  async createForm(fieldsData: { field_name: string; field_type: string }[]) {
    try {
      return await this.formFieldModel.bulkCreate(fieldsData);
    } catch (error) {
      throw new Error(`Failed to fetch user: ${error.message}`);
    }
  }

  getFormTitleDetails(formTitle: string) : Promise<any> {
    const forms =  this.formModel.findOne({
      where: { title: formTitle },
    });
    return forms;
  }

  /**
   * processFormData() :- This function is created for fill data in dynamic created form.
   * @param formTitle It's query string form title which is passed in API parameter.
   * @param formData It's DTO object for insert form_field table.
   * @returns The insert data Object will return.
   */
  async processFormData(formTitle: string, formData: any): Promise<any> {
    const forms = await this.getFormTitleDetails(formTitle);
    const uniqueid =  forms ? forms.uniqueid : null;
    
    if (uniqueid != null) {
      const data = await this.formFieldModel.findAll({
        where: { form_id: uniqueid },
      });

      console.log(formData);
      data.forEach((dataItem) => {
        for (const key in formData) {
          if (key == dataItem.field_name) {
            const formValue = formData[key];
            this.formDataModel.create({
              form_id: forms.id,
              form_field_id: dataItem.id,
              form_value: formValue,
            });
          }
        }
    });
      return data;
    }
  };

  /**
   * displayFormData() :- This function is created for Fetching data based on dynamic created form.
   * @param formTitle It's query string form title which is passed in API parameter.
   * @returns The data Object will return.
   */
  async displayFormData(formTitle: string): Promise<any> {
    try {
      const dispData = [];
      const forms = await this.getFormTitleDetails(formTitle);
      const uniqueid =  forms ? forms.uniqueid : null;
      
      const data =  await this.formDataModel.findAll({
        where: { form_id: forms.id},
        attributes: ['id','form_field_id','form_value'], 
      });

      const dataField =  await this.formFieldModel.findAll({
        where: { form_id: uniqueid},
        attributes: ['id','field_name','field_type'],
      });
      
      dataField.forEach((dataItem) => {
        data.forEach((item) => {
          if (dataItem.id == item.form_field_id) {
            if (dataItem.field_name != null) {
              dispData[item.id] = [dataItem.field_name , item.form_value];
            }
          }
        });

      });
      return dispData;
    } catch (error) {
      throw new Error(`Failed to fetch user: ${error.message}`);
    }
  }
  
}
