import {
  Controller,
  Get,
  Post,
  Param,
  Body,
  HttpStatus,
  HttpCode,
  UsePipes,
  NotFoundException,
  ValidationPipe,
  BadRequestException,
  Query 
} from '@nestjs/common';
import { FormService } from './form.service';
import { Form } from './entity/form.entity';
import { CreateFormDto } from './dto/create-form.dto';
import { CreateFormDataDto } from './dto/create-form-data.dto';
import {Form_Field} from "./entity/form_field.entity";


@Controller('form')
export class FormController {
  constructor(private readonly formService: FormService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)

   /**
   * createForm() : - Create dynamic generate form.
   * @param createFormDto It's DTO object for insert in DB field column.
   * @returns The Form object Insert.
   */
  async createForm(@Body() createFormDto: CreateFormDto) {
    try {
     const { fields } = createFormDto;

      return this.formService.createForm(fields);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  @Post('/fill_data')
  @UsePipes(new ValidationPipe({ transform: true, whitelist: true, forbidNonWhitelisted: true }))
  /**
   * fillFormData() :- This function is created for fill data in dynamic created form.
   * @param formTitle It's query string form title which is passed in API parameter.
   * @param CreateFormDataDto It's DTO object for insert form_field table.
   * @returns The insert data Object will return.
   */
  async fillFormData(@Query('form_title') formTitle: string, @Body() CreateFormDataDto: CreateFormDataDto): Promise<any> {
    try {
      const result = await this.formService.processFormData(formTitle, CreateFormDataDto);
      return result;
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  @Get('/fill_data')
  /**
   * getFormData() :- This function is created for Fetching data based on dynamic created form.
   * @param formTitle It's query string form title which is passed in API parameter.
   * @returns The data Object will return.
   */
  async getFormData(@Query('form_title') formTitle: string): Promise<any> {
    try {
      const result = await this.formService.displayFormData(formTitle);
      console.log(result);
      return result;
    }  catch (error) {
      throw new BadRequestException(error.message);
    }

  }

}
