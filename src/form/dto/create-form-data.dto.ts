import { IsNotEmpty, IsString,IsNumber,IsEmail,IsArray,ArrayMinSize } from 'class-validator';
import { Type } from 'class-transformer';

export class CreateFormDataDto {

  @IsNotEmpty({ message: 'Name is required' })
  @IsString({ message: 'Name must be String only.' })
  name: string;

  @IsNotEmpty({ message: 'Email is required' })
  @IsEmail({}, { message: 'Email format is Invalid' })
  email: any;

  @IsNotEmpty({ message: 'Phone Number is required' })
  @IsNumber({},{ message: 'Phone Number must be number only' })
  phonenumber: bigint;

  @IsNotEmpty({ message: 'isGraduate is required' })
  @IsNumber({}, { message: 'isGraduate should be number only' })
  isGraduate: boolean;
}