import { IsNotEmpty, IsString,IsNumber,IsEmail,IsArray,ArrayMinSize } from 'class-validator';

export class CreateFormDto {

  @IsArray()
  @ArrayMinSize(1)
  fields: { field_name: string; field_type: string }[];
}