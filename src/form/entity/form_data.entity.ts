import { Column, Model, Table,HasMany } from 'sequelize-typescript';
import {Form_Field} from "./form_field.entity";

@Table({ tableName: 'form_data' })
export class Form_Data extends Model<Form_Data> {

  @Column({allowNull: false})
  form_id: number;

  @Column({allowNull: false})
  form_field_id: number;

  @Column({ type: 'varchar(50)', allowNull: false})
  form_value: string;

}