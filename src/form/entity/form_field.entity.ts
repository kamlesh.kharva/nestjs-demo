import { Column, Model, Table } from 'sequelize-typescript';

@Table({ tableName: 'form_field' })
export class Form_Field extends Model<Form_Field> {

  @Column({ allowNull: false })
  form_id: number;

  @Column({ type: 'varchar(50)', allowNull: false})
  field_name: string;

  @Column({ type: 'varchar(50)', allowNull: false})
  field_type: string;
}