import { Column, Model, Table } from 'sequelize-typescript';
import { v4 as uuid } from 'uuid';

@Table({ tableName: 'form' })
export class Form extends Model<Form> {

  @Column({ allowNull: false })
  uniqueid: string  = uuid();

  @Column({type: 'varchar(50)', allowNull: false, defaultValue: 'user'})
  title: string;
}