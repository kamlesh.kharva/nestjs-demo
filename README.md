﻿
**Nest.js Form API Documentation**

This documentation outlines the structure, functionality, and best practices used in a Nest.js application that implements a RESTful API for form management, incorporating design patterns, SOLID principles, and database interactions with Sequelize.

**Installation**

Provide instructions on how to install and run your Nest.js project locally

1) git clone <repository\_url>
1) Install dependencies using command line

npm install

1) Setup environment variable 
1) Start Application using cmd

npm run start:dev

**Usage**

Explain how to use your API and interact with the endpoints.

1) Access the API endpoints using tools like Postman or CURL.
1) We have used swagger for API documentation.
1) Use appropriate HTTP methods (GET, POST, etc.) to interact with the forms.




**API Endpoints**

- **POST /forms**: Create a new form
- **POST /form/fill\_data?form\_title=Form\_1**: Insert data in dynamic created form.
- **GET /form/fill\_data?form\_title=Form\_1**: Get data for particular form which is passed in argument.

**Request and Response API**

- **POST /forms**

**Request**

{

`  `"fields": [

`    `{  "field\_name": "name", "field\_type": "string" },

`    `{  "field\_name": "email", "field\_type": "string" },

`    `{  "field\_name": "phonenumber", "field\_type": "bigint" },

`    `{  "field\_name": "isGraduate", "field\_type": "boolean" }

`  `]

}


- **POST /form/fill\_data?form\_title=Form\_1**:

{

"name":"CHeckTEST",

"email":"check@gmail.com",

"phonenumber" : 12251444,

"isGraduate":1

}








- **GET /form/fill\_data?form\_title=Form\_1**

{

`    `{   "name",

`        `"Testing12644"

`    `},

`    `{

`        `"email",

`        `"tes22t@gmail.com"

`    `},

`    `{

`        `"phonenumber",

`        `"12251444"

`    `},

`    `{

`        `"isGraduate",

`        `"1"

`    `}

}


**Design Patterns and Principles**

- **MVC Architecture**: Separation of concerns with Models, Views (not implemented for an API), and Controllers.
- **Dependency Injection**: Leveraging Nest.js DI for injecting services into controllers.
- **Repository Pattern**: Abstraction of database operations in the service layer.
- **DTOs and Validation**: Using Data Transfer Objects and class-validator for input validation.
- **SOLID Principles**:
- Single Responsibility Principle: Each class/module has a single responsibility.
- Open/Closed Principle: Modules are open for extension but closed for modification.

**Technologies/feature Used**

- Nest.js
- Sequelize
- TypeScript
- Class-validator
- Swagger for API Documentation



**Contributing**

Provide guidelines for contributing to the project.

1) Fork the repository.
1) Create a new branch (**git checkout -b feature/form**).
1) Commit your changes (**git commit -am 'Add new form**).
1) Push to the branch (**git push origin feature/form**).
1) Create a new Pull Request.

**License**

This project is licensed under the MIT License.



